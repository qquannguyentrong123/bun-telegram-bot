import fs from 'fs'

export const LogFile = (message:string) => {
    const logData = `${message}\n\n`;

    fs.appendFile('logs.log', logData, { flag: 'a+' }, (err) => {
        if (err) {
          if (err.code === 'ENOENT') {
            fs.writeFile('logs.log', logData, (createErr) => {
              if (createErr) {
                console.error(createErr);
              } 
            });
          } else {
            console.error(err);
          }
        } 
      });
}