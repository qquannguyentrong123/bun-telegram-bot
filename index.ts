const TelegramBot = require('node-telegram-bot-api');
import { google } from "googleapis";
import { LogFile } from "./service";
import { renderToReadableStream } from "react-dom/server";

const PORT = process.env.PORT
const TELEGRAM_TOKEN = process.env.TELEGRAM_TOKEN

const { CLIENT_ID, CLIENT_SECRET, REDIRECT_URI, REFRESH_TOKEN, GG_DRIVE_FOLDER_ID, IMAGE_NAME } = process.env;

const oauth2Client = new google.auth.OAuth2(
  CLIENT_ID,
  CLIENT_SECRET,
  REDIRECT_URI
);
oauth2Client.setCredentials({ refresh_token: REFRESH_TOKEN });

const drive:any = google.drive({
  version: "v3",
  auth: oauth2Client,
});


const bot = new TelegramBot(TELEGRAM_TOKEN, { polling: true, } );

bot.on('poll_answer', (polling:any) => {
  console.log(polling)
});

bot.on('poll', (msg: any) => {
  const chatId = msg?.chat?.id;
  console.log(msg?.poll?.id)
} );

bot.on('message', (msg:any) => {
  const chatId = msg.chat.id;
  if(msg.photo) {
    const photo = msg?.photo[2];
    const fileId = photo.file_id;
    bot.getFile(fileId).then((fileUrl:any) => {
      const fileStream:any = bot.getFileStream(fileId);
      const fileName:any = IMAGE_NAME?.replace('{{NAME}}', fileUrl.file_unique_id).replace('{{DATE}}', msg.d);
      const fileMetadata:any = {
          name: fileName,
          parents: [GG_DRIVE_FOLDER_ID]
        };
        const media = {
          mimeType: 'image/webp',
          body: fileStream,
        };

         drive.files.create(
          {
            resource: fileMetadata,
            media: media,
          },
         async (err:any, file:any) => {
            if (err) {
              console.error(err);
              bot.sendMessage(chatId, 'Có lỗi xảy ra khi tải ảnh lên Google Drive, kiểm tra lại token của gg drive');
            } else {
              const getUrl = await setFilePublic(file?.data?.id);
              const logMessage = `${msg.from.username} -  ${new Date(msg.date * 1000)} - ${getUrl.data.webViewLink}`;
              LogFile(logMessage);
            }
          }
        );
    });
  }
});


bot.on("polling_error", (error:any) => {
  console.error(error);
});

const dt = new Intl.DateTimeFormat();

const setFilePublic = async (fileId:string) => {
  try {
    await drive.permissions.create({
      fileId,
      requestBody: {
        role: "reader",
        type: "anyone",
      },
    });

    const getUrl = await drive.files.get({
      fileId,
      fields: "webViewLink, webContentLink",
    });

    return getUrl;
  } catch (error) {
    console.error(error);
  }
};

const server = Bun.serve({
    port: PORT,
    fetch(req) {
      const url:string = (new URL(req.url)).pathname;
        switch (url) {
          case '/': 
          return new Response(Bun.file("layout.html"), {
            headers: {
              "Content-Type": "text/html",
            },
          });
          case '/create': 
            return new Response("Hello");
          default: 
            return new Response("404!");
        }
    },
});
  
console.log(`Listening on http://localhost:${server.port} ...`);